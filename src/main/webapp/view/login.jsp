<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>User-Login</title>
<%@include file="components/common_css_js.jsp"%>
</head>
<body>
	<%@include file="components/navbar.jsp"%>
	<div class="container">
		<div class="row">
			<div class="col-md-6 offset-md-4">
				<div class="card mt-3">
					<div class="card-header ">
						<h3>Login here</h3>
					</div>

					<div class="card-body">

						<div class="container text-center">
							<img style="max-width: 90px" class="img-fluid rounded-circle"
								src="https://cdn-icons-png.flaticon.com/512/554/554846.png"
								alt="login" />
						</div>
						<div class=" alert-dismissible fade show"
							role="alert">
							<strong>${error}</strong>
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form action="signin" method="post" modelAttribute="b">
							<div class="form-group">
								<label for="exampleInputEmail1">Email </label> <input
									name="email" type="email" class="form-control"
									id="exampleInputEmail1" aria-describedby="emailHelp"
									placeholder="Enter email"> <small id="emailHelp"
									class="form-text text-muted"> your email is secure with
									us.</small>
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Password</label> <input
									name="password" type="password" class="form-control"
									id="exampleInputPassword1" placeholder="Password">
							</div>
							<div class="container text-center">
								<button type="submit" class="btn btn-primary">Login</button>
								<button type="reset" class="btn btn-warning">Reset</button>
							</div>
						</form>
					</div>



				</div>

			</div>
		</div>

	</div>
</body>
</html>