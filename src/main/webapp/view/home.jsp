<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>

<%@include file="components/common_css_js.jsp"%>
</head>
<body>

	<%@include file="components/navbar.jsp"%>

	<hr />

	<div class="row">
		<div class="col-md-6">
			<h1
				style="font-family: flamaregular; font-weight: 400; font-size: 25px; color: #bf1e71; margin: 0 0 5px;">ABOUT
				US</h1>
			<p align="justify">PVR Ltd. is the market leader in terms of
				screen count in India. Since 1997, the brand has redefined the
				cinema industry and the way people watch movies in the country. The
				Company has, over the years, consistently added screens, both
				organically and inorganically, through strategic investments and
				acquisitions which includes "Cinemax Cinemas" in November 2012, "DT
				Cinemas" in May 2016 and "SPI Cinemas" in August 2018 which added
				138 screens, 32 screens and 76 screens respectively to our screen
				network. Currently, we operate 846 screens in 176 cinemas in 71
				cities in India and Sri Lanka with an aggregate seating capacity of
				approximately 1.82 lakhs seats. We offer a diversified cinema
				viewing experience through our formats, including PVR Directors Cut,
				PVR LUXE, PVR IMAX, PVR Superplex, PVR Playhouse, PVR ECX, PVR
				Premiere, PVR ICON, PVR LUX, PVR Cinemas and PVR Utsav, and pursuant
				to our acquisition and amalgamation of SPI Cinemas, Escape, Sathyam
				and Palazzo. The Company exhibits diversified content to serve
				different regional customer segments across India. We have a
				diversified revenue stream and generate revenues primarily from box
				office and non-box office which primarily includes revenue from Sale
				of Food and Beverages, advertisement income, convenience fees, and
				income from movie production/ distribution among others.</p>

		</div>
		<div class="col-md-6">
			<div id="carouselExampleIndicators" class="carousel slide"
				data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0"
						class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100"
							src="https://images.news18.com/ibnlive/uploads/2017/01/PVR.jpg?im=Resize,width=360,aspect=fit,type=normal?im=Resize,width=320,aspect=fit,type=normal"
							alt="First slide">
					</div>
	
				</div>
			
			</div>

		</div>


	</div>
</body>
</html>





