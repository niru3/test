<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update User</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<%@include file="components/generalbar.jsp"%>

	
	
	
	<div class="card">


	<div class="container text-center">
		<h3 class="card-header">Update the User Details</h3>
		<table class="table table-striped table-bordered" style="width:100px">

			<s:form action="updatedUserValue" method="post" modelAttribute="b">
			
				
				<div class="form-group">
				<input type="text" class="form-control" name="id" value="${id}"
						required />
				</div>
				
				<div class="form-group">
					<input type="text" class="form-control" name="name" value="${name}"
						placeholder="Enter Name"  required />
				</div>
				
				<div class="form-group">
					<input type="number" class="form-control" name="mobile"
						value="${mobile}" placeholder="Enter City"  required />
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="email"
						value="${email}" placeholder="Enter Email" required />
				</div>
				<div class="form-group">
					<input type="text" class="form-control"
						name="password" value="${password}" placeholder="Enter Password"  required />
				</div>
				<div class="form-group">
					<input type="text" class="form-control"
						name="city" value="${city}" placeholder="Enter City"  required />
				</div>

				<div class="container text-center">
					<button class="btn btn-outline-success">Edit</button>
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>


			</s:form>

		</table>
	</div>
	</div>
	
</body>
</html>