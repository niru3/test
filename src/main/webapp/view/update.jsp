<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<%@include file="components/generalbar.jsp"%>
	<div class="card">


	<div class="container text-center">
		<h3 class="card-header">Update the Booking Details</h3>
		<table class="table table-striped table-bordered" style="width:100px">

			<s:form action="updatedValue" method="post" modelAttribute="b">

				<div class="form-group">
				<input type="text" class="form-control" name="id" value="${id}"
						placeholder="Enter Your LogIn ID" required />
				</div>
				
				<div class="form-group">
					<input type="text" class="form-control" name="name" value="${name}"
						placeholder="Enter Your Name" required />
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="movie" value="${movie}"
						placeholder="Enter Movie" required />
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="showDate"
						value="${showDate}" placeholder="Enter Show Date" required />
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="showTime"
						value="${showTime}" placeholder="Enter Show Time" required />
				</div>
				<div class="form-group">
					<input type="number" min="1" max="5" class="form-control"
						name="tickets" value="${tickets}" placeholder="Enter No. Of Tickets" required />
				</div>
				<div class="container text-center">
					<button class="btn btn-outline-success">Update Booking</button>
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>


			</s:form>

		</table>
	</div>
	</div>
</body>
</html>



