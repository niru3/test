<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<style>
body {
	background-color: powderblue;
}

h1 {
	color: blue;
}

p {
	color: red;
}

.custom-bg {
	background: #673ab7 !important;
}

.admin .card {
	border: 1px solid red;
}

.admin .card:hover {
	background: #e2e2e2;
	cursor: pointer;
}
</style>
<meta charset="ISO-8859-1">
<title>User Page</title>
<%@include file="components/common_css_js.jsp"%>
</head>
<body>
	<%@include file="components/userbar.jsp"%>

<div class="container admin">
	<%@include file="components/message.jsp"%>

	

	<div class="row mt-3">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body text-center">
					<div class="container">
						<a href="userdetail"> <img style="max-width: 125px"
							class="img-fluid rounded-circle"
							src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwL7XDJ15xa2G6Ios0PX0mzmtop2QsDjQmltyv_sBb&s"
							alt="user_icon"></a>
					</div>
					<h1>Profile</h1>

				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-body text-center">
					<div class="container">
						<a href="booking"> <img style="max-width: 125px"
							class="img-fluid rounded-circle"
							src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRx52whHqSxYfm9WU2D1CXee_7H2LgMmI_muJdDCULC&s"
							alt="user_icon"></a>
					</div>
					<h1>Booking</h1>

				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
				<div class="card-body text-center">
					<div class="container">
						<a href="servicesinfo"> <img style="max-width: 125px"
							class="img-fluid rounded-circle"
							src="https://images.freecreatives.com/wp-content/uploads/2017/10/flat-clapperboard-icon_1063-38.jpg"
							alt="user_icon"></a>
					</div>
					<h1>Movies</h1>

				</div>
			</div>

		</div>
	</div>
	</div>


	<!--  Password-->


	<div class="modal fade" id="change-password-modal" tabindex="-1"
		role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header custom-bg text-white">
					<h5 class="modal-title" id="exampleModalLabel">Update Password</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<form action="userchangepassword" method="post" modelAttribute="b">
						<div class="form-group">
							<input type="password" class="form-control" name="pr_password"
								placeholder="Enter old password" required />
						</div>
						<div class="form-group">
							<input type="password" class="form-control" name="new_password"
								placeholder="Enter new password" required />
						</div>

						<div class="container text-center">
							<button class="btn btn-outline-success">Change Password</button>
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Close</button>

						</div>

					</form>

				</div>

			</div>
		</div>
	</div>



	<!--end password  -->


	<!--book stay  -->


	<div class="modal fade" id="add-guest-modal" tabindex="-1"
		role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header custom-bg text-white">
					<h5 class="modal-title" id="exampleModalLabel">Fill guest
						details</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">

					<form action="bookastay" method="post" modelAttribute="b">
						<div class="form-group">
							<input type="text" class="form-control" name="name"
								placeholder="Enter name" required />
						</div>
						<div class="form-group">
							<input type="date" class="form-control" name="check_in"
								placeholder="Enter Check in" required />
						</div>
						<div class="form-group">
							<input type="date" class="form-control" name="check_out"
								placeholder="Enter Check out" required />
						</div>
						<div class="form-group">
							<input type="number" min="1" max="5" class="form-control"
								name="room" placeholder="Enter Rooms" required />
						</div>
						<div class="form-group">
							<input type="number" min="1" max="10" class="form-control"
								name="nosguest" placeholder="Enter no. of guest" required />
						</div>

						<div class="container text-center">
							<button class="btn btn-outline-success">Add Guest</button>
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Close</button>

						</div>

					</form>

				</div>

			</div>
		</div>
	</div>




</body>
</html>