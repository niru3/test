<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Bookings</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>

	<%@include file="components/generalbar.jsp" %>
	
	
	<div class="alert alert-success alert-dismissible fade show"
				role="alert">
				<strong>${msg}</strong>
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
<!-- 	<div style="width: 800px"> -->
	<div class="container">
	<h4>ALL BOOKINGS</h4>
		<table class="table table-striped table-bordered" style="width: 100%">
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Movie</th>
				<th>Show Date</th>
				<th>Show Time</th>
				<th>No. Of Tickets</th>
				
				<th>Bill</th>
			</tr>
			<c:forEach items="${b}" var="guest">
				<tr>
					<td>${guest.getId()}</td>
					<td>${guest.getName()}</td>
					<td>${guest.getMovie()}</td>
					<td>${guest.getShowDate()}</td>
					<td>${guest.getShowTime()}</td>
					<td>${guest.getTickets()}</td>
					<td><a href="bill?id=${guest.getId()}&name=${guest.getName()}&movie=${guest.getMovie()}&showDate=${guest.getShowDate()}&showTime=${guest.getShowTime()}&tickets=${guest.getTickets()}">Generate Bill</a></td>
				</tr>


			</c:forEach>



		</table>
	</div>
	



</body>
</html>