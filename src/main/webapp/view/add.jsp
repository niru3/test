<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="s"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Add</title>
<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
</head>
<body>
	<%@include file="components/generalbar.jsp"%>
	<div class="card">


		<div class="container text-center">
			<h3 class="card-header">Fill Booking Details</h3>
			<table class="table table-striped table-bordered"
				style="width: 100px">
				<s:form action="addGuest" method="post" modelAttribute="b">
					<div class="form-group">
						<input type="text" class="form-control" name="id" value="${id}"
							placeholder="Enter ID"  required />
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="movie"
							value="${movie}" placeholder="Enter Movie"  required />
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="name"
							value="${name}" placeholder="Enter Name"  required />
					</div>

					<div class="form-group">
						<input type="text" class="form-control" name="showDate"
							value="${showDate}" placeholder="Enter Show Date" required />
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="showTime"
							value="${showTime}" placeholder="Enter Show Time" required />
					</div>
					<div class="form-group">
						<input type="number" class="form-control" name="tickets"
							value="${tickets}" placeholder="Enter No. of Tickets Required"  required />
					</div>
					<div class="container text-center">
						<button class="btn btn-outline-success">Book</button>
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
					</div>


				</s:form>

			</table>

		</div>
	</div>

</body>
</html>