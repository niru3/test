<%String message=(String)session.getAttribute("current-user"); %>

<nav class="navbar navbar-expand-lg navbar-dark" style="background:#3ab78f">
	<div class="container">
		<a class="navbar-brand" href="home">PVR CINEMA</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse"
			data-target="#navbarSupportedContent"
			aria-controls="navbarSupportedContent" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="home">Home
						<span class="sr-only">(current)</span>
				</a></li>
         </ul>
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a class="nav-link"
					href="#"><%=message%></a></li>
				<li class="nav-item active"><a class="nav-link"
					href="login" data-toggle="modal" data-target="#change-password-modal">Change Password</a></li>
				<li class="nav-item active"><a class="nav-link"
					href="logout">Logout</a></li>

			</ul>
		</div>

	</div>
</nav>