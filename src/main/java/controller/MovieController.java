package controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import dao.IMovie;
import dao.MovieImpl;
import model.Movie;
import model.User;

@Controller
public class MovieController {

	IMovie movie = new MovieImpl();
	
	
	@RequestMapping("deleteservice")
	public ModelAndView delete(@RequestParam("id") int id) {
		ModelAndView view = new ModelAndView("services");
		view.addObject("msg", movie.delete(id));
		view.addObject("b", movie.viewService());
		return view;

	}
	@RequestMapping("updatedServices")
	public ModelAndView updatedService(@ModelAttribute("b") Movie b) {
		ModelAndView view = new ModelAndView("services");
		view.addObject("msg",movie.update(b));
		view.addObject("b", movie.viewService());
		return view;

	}
	
	@RequestMapping("movie")
	public ModelAndView updateservice(@RequestParam("id") int id,@RequestParam("title") String title,@RequestParam("showDate") String showDate,@RequestParam("showTime") String showTime,@RequestParam("price") Long price) {
		ModelAndView view = new ModelAndView("movie");
		view.addObject("id", id);
		view.addObject("title", title);
		view.addObject("showDate",showDate);
		view.addObject("showTime",showTime);
		view.addObject("price",price );
		view.addObject("b", new User());
		//view.addObject("b", movie.viewService());
		return view;

	}
	@RequestMapping("updatedMovieValue")
	public ModelAndView updatedUserValue(@ModelAttribute("b") Movie b) {
		ModelAndView view = new ModelAndView("services");
		view.addObject("msg", movie.update(b));
		view.addObject("b", movie.viewService());
		return view;

	}
	
	@RequestMapping("services")
	public ModelAndView admin() {
		return new ModelAndView("services","b",movie.viewService());
	}
	
	@RequestMapping("servicesinfo")
	public ModelAndView serviceinfo() {
		return new ModelAndView("servicesinfo","b",movie.viewService());
	}
	
	
	@RequestMapping("addService")
	public ModelAndView addService(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("b") Movie b) throws IOException {
		ModelAndView view = new ModelAndView("bill");
		view.addObject("msg", movie.add(b));

		view.addObject("b", movie.viewService());
		HttpSession httpsession=request.getSession();
		httpsession.setAttribute("message", "Movie added successfully");


		return new ModelAndView("admin");

	}
	
	
	
}
