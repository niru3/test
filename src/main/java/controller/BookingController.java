package controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import dao.BookingImpl;
import dao.IBooking;
import dao.UserImpl;
import model.Booking;
import model.Movie;
import model.User;

@Controller
public class BookingController {
	Booking gues=new Booking();
	IBooking booking = new BookingImpl();
	UserImpl userImpl=new UserImpl();
	Movie mov = new Movie();

	@RequestMapping("about")
	public ModelAndView about() {
		return new ModelAndView("about");// about.jsp
	}
	
	@RequestMapping("register2")
	public ModelAndView register2() {
		return new ModelAndView("register2");// about.jsp
	}
	
	@RequestMapping("showGuests")
	public ModelAndView showGuests() {
		return new ModelAndView("showGuests", "b", booking.viewGuests());
	}
	
	@RequestMapping("booking")
	public ModelAndView booking(HttpServletRequest request,HttpServletResponse response,HttpSession session,@ModelAttribute("b") Booking b) {
		return new ModelAndView("booking", "b", booking.getGuestByUsername((String)session.getAttribute("name")));
	}

	@RequestMapping("home")
	public ModelAndView home() {
		return new ModelAndView("home");// home.jsp
	}

	@RequestMapping("login")
	public ModelAndView login() {
		return new ModelAndView("login");
	}
	@RequestMapping(path = "signin",method = RequestMethod.POST)
	public String signin(@RequestParam("email") String email,@RequestParam("password") String password,Model model,HttpSession session) {
		User user= userImpl.getUserByUsername(email);
		if(user==null) {
			model.addAttribute("error","Incorrect email or password!");
			return "login";
		}
		if(!(user.getPassword().equals(password))){
			model.addAttribute("error","Incorrect email or password!");
			return "login";
		}
		session.setAttribute("current-email", email);

		session.setAttribute("current-user", user.getName());
		return "redirect:/normal";
	}
	

	

	@RequestMapping("view")
	public ModelAndView viewGuests() {
		return new ModelAndView("showGuests", "b", booking.viewGuests());
	}

	@RequestMapping("delete")
	public ModelAndView delete(@RequestParam("id") int id) {
		ModelAndView view = new ModelAndView("showGuests");
		view.addObject("msg", booking.delete(id));
		view.addObject("b", booking.viewGuests());
		return view;

	}

	@RequestMapping("update")
	public ModelAndView update(@RequestParam("id") int id, @RequestParam("name") String name,
			@RequestParam("showDate") String showDate, @RequestParam("showTime") String showTime,
			@RequestParam("tickets") int tickets) {
		ModelAndView view = new ModelAndView("update");// update.jsp
		view.addObject("id", id);
		view.addObject("name", name);
		view.addObject("showDate", showDate);
		view.addObject("showTime",showTime);
		view.addObject("tickets", tickets);
		view.addObject("b", new Booking());
		return view;

	}

	@RequestMapping("updatedValue")
	public ModelAndView updatedValue(@ModelAttribute("b") Booking b) {
		ModelAndView view = new ModelAndView("showGuest");
		view.addObject("msg", booking.update(b));
		//view.addObject("b", booking.viewGuests());
		return view;
		

	}

	@RequestMapping("add")
	public ModelAndView add() {
		return new ModelAndView("add", "b", new Booking());// add.jsp

	}

	@RequestMapping("addGuest")
	public ModelAndView addGuest(HttpServletRequest request,HttpServletResponse response,@ModelAttribute("b") Booking b) {
		ModelAndView view = new ModelAndView("bill");
		String str=request.getParameter("name");
		String l = request.getParameter("id");
		String m = request.getParameter("tickets")	;
		Integer k = Integer.decode(m) * 200;
		System.out.println(str);
		view.addObject("msg",booking.add(b));
		System.out.println(gues.getId());
		HttpSession httpsession=request.getSession();
	
		httpsession.setAttribute("name", str);
		httpsession.setAttribute("id", l);
		httpsession.setAttribute("tickets",m);
		httpsession.setAttribute("result",k);
		return new ModelAndView("bill");// add.jsp

	}
	



	@RequestMapping("bill")
	public ModelAndView bill(@RequestParam("id") int id, @RequestParam("name") String name,
			@RequestParam("showDate") String showDate, @RequestParam("showTime") String showTime,
			@RequestParam("tickets") int tickets ) {

		ModelAndView view=new ModelAndView("bill");
		long k = tickets* 200;
		view.addObject("result",k);
		view.addObject("id", id);
		view.addObject("name", name);
		view.addObject("showDate", showDate);
		view.addObject("showTime",showTime);
		view.addObject("tickets", tickets);
		view.addObject("b", new Booking());		
		return  view;
	}

}
