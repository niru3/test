package dao;

import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

import model.Movie;
import util.Query;

public class MovieImpl implements IMovie{

	@Override
	public List<Movie> viewService() {
		// TODO Auto-generated method stub
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		@SuppressWarnings("unchecked")
		List<Movie> list = manager.createQuery(Query.VIEW_MOVIE).getResultList();

		return list;
	}

	@Override
	public String delete(int id) {
		// TODO Auto-generated method stub
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		Movie b = manager.getReference(Movie.class, id);
		manager.remove(b);
		manager.getTransaction().commit();
		return id + " Removed From Book List";
	}

	@Override
	public String update(Movie movie) {
		// TODO Auto-generated method stub
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		manager.merge(movie);
		manager.getTransaction().commit();
		return movie.getId() + " Updated";
	}

	@Override
	public String add(Movie movie) {
		// TODO Auto-generated method stub
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		manager.persist(movie);
		manager.getTransaction().commit();
		return movie.getId() + " Added";
	}

}
