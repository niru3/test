package dao;

import java.util.List;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import model.Booking;
import util.Query;

public class BookingImpl implements IBooking {

	@Override
	public List<Booking> viewGuests() {
		// TODO Auto-generated method stub

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		@SuppressWarnings("unchecked")
		List<Booking> list = manager.createQuery(Query.VIEW_BOOKING).getResultList();

		return list;
	}

	@Override
	public String delete(int id) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		Booking b = manager.getReference(Booking.class, id);
		manager.remove(b);
		manager.getTransaction().commit();
		return id + " Removed From Book List";

	}

	@Override
	public String update(Booking booking) {
		// TODO Auto-generated method stub
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		manager.merge(booking);
		manager.getTransaction().commit();
		return booking.getId() + " Updated";
		
	}

	@Override
	public String add(Booking booking) {
		// TODO Auto-generated method stub
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("hcl");
		EntityManager manager = factory.createEntityManager();

		manager.getTransaction().begin();
		manager.persist(booking);
		manager.getTransaction().commit();
		return booking.getId() + " Added";
	}
	
	@Override
	public List<Booking> getGuestById(int id) {
		
		EntityManagerFactory factory=Persistence.createEntityManagerFactory("hcl");
		EntityManager manager=factory.createEntityManager();
		try {
		@SuppressWarnings("unchecked")
		List<Booking> booking=manager.createQuery("select u from Booking u where u.id='"+id+"'").getResultList();
		if(booking.size()==0) return null;
		@SuppressWarnings("unused")
		Booking user = null;
		for(Booking user2:booking) {
			user=user2;
		}
		return booking;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			manager.close();
			factory.close();
		}
	}
	
	@Override
	public List<Booking> getGuestByUsername(String name) {
		EntityManagerFactory factory=Persistence.createEntityManagerFactory("hcl");
		EntityManager manager=factory.createEntityManager();
		try {
		@SuppressWarnings("unchecked")
		List<Booking> users=manager.createQuery("select u from Booking u where u.name='"+name+"'").getResultList();
		if(users.size()==0) return null;
		@SuppressWarnings("unused")
		Booking user = null;
		for(Booking user2:users) {
			user=user2;
		}
		return users;
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			manager.close();
			factory.close();
		}
	}
	


}
