package dao;

import java.util.List;

import model.Booking;

public interface IBooking {
	
	public List<Booking> viewGuests();
	public String delete(int id);
	
	public String update(Booking booking);
	public String add(Booking booking);
	public List<Booking> getGuestById(int id);
	public List<Booking> getGuestByUsername(String name);

}
