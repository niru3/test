package dao;

import java.util.List;


import model.Movie;

public interface IMovie {
	
	
	public List<Movie> viewService();
	public String delete(int id);
	
	public String update(Movie movie);
	public String add(Movie movie); 

}
