use hcl2;
drop table bookings;

ALTER TABLE bookings RENAME booking;
select * from booking;
show tables;
insert into user(id,name,email,password,mobile,city) values('Ashu','Ashu@gmail.com','Ashu@12',9898980212,'Hyderabad');
insert into booking(id,movie,name,showDate,showTime,tickets) values(6,'Pathan','Ashu','13-01-23','7:00',2);

Alter table movie DROP COLUMN image;
create table movie(id int primary key auto_increment,title varchar(100) not null,showDate varchar(15)not null,showTime varchar(15)not null,price bigint not null );
insert into movie(id,title,showDate,showTime,price) values(1,'Pathaan','23-01-23','7:00',200);
insert into movie(id,title,showDate,showTime,price) values(2,'The Woman King','25-01-23','8:00',200);
insert into movie(id,title,showDate,showTime,price) values(3,'Daman','25-01-23','8:00',200);
insert into movie(id,title,showDate,showTime,price) values(4,'Ved','26-01-23','11:00',200);
insert into movie(id,title,showDate,showTime,price) values(5,'Michael','2a7-01-23','9:00',200);
select * from booking;
use hcl2;
drop table bookings;
UPDATE movies
SET price = 200
WHERE id=2;
drop table movies;
desc table booking;
desc booking;
select * from booking;
ALTER TABLE booking DROP PRIMARY KEY;
DELETE FROM booking WHERE id=9;
use hcl2;
create table booking( id int not null,movie varchar(30) not null,name varchar(30) not null,showDate varchar(15) not null,showTime varchar(15) not null,tickets int  not null);
insert into booking(id,movie,name,showDate,showTime,tickets) values(4,'Daman','Sudhir','25-01-23','8:00',2);
drop table movies;